package br.inatel.dashboard.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BodyNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 3075021119356542991L;

    public BodyNotFoundException(String message) {
        super(message);
    }

}
