package br.inatel.dashboard.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.Widget;
import br.inatel.dashboard.repository.WidgetRepository;

@Service("widgetService")
public class WidgetService {

	private WidgetRepository widgetRepository;

	@Autowired
	public WidgetService(WidgetRepository widgetRepository) {
		this.widgetRepository = widgetRepository;
	}

	public void save(Widget widget) {
		widgetRepository.save(widget);
	}

	public List<Widget> findByDashboardId(String objectId) {

		return widgetRepository.findByDashboardId(objectId);
	}

	public void remove(Widget widget) {
		widgetRepository.delete(widget);
	}

	public void ediWidgetSize(String objectId, String height, String width, String username) {

		Optional<Widget> widget = widgetRepository.findByIdAndUsername(objectId, username);

		if (widget.isPresent()) {
			widget.get().setHeight(height);
			widget.get().setWidth(width);
			widgetRepository.save(widget.get());
		}

	}
	
	public void removeWidget(String objectId, String username) {

		Optional<Widget> widget = widgetRepository.findByIdAndUsername(objectId, username);

		if (widget.isPresent()) {
			widgetRepository.delete(widget.get());
		}

	}
	
	public void removeWidgetByDashboardId(String dashboardId) {

		widgetRepository.deleteByDashboardId(dashboardId);
	}

}
