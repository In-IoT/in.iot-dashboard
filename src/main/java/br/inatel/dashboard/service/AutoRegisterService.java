package br.inatel.dashboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.AutoRegister;
import br.inatel.dashboard.repository.AutoRegisterRepository;

@Service("autoRegisterService")
public class AutoRegisterService {

	private AutoRegisterRepository autoRegisterRepository;

    @Autowired
    public AutoRegisterService(AutoRegisterRepository autoRegisterRepository) {
        this.autoRegisterRepository = autoRegisterRepository;
    }
    
    public void save(AutoRegister autoregister) {
    	this.autoRegisterRepository.save(autoregister);
    }
    
    public AutoRegister findByUsername(String username) {
    	return this.autoRegisterRepository.findByUsername(username);
    }
}
