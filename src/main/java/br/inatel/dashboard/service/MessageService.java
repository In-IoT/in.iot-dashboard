package br.inatel.dashboard.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.Message;
import br.inatel.dashboard.repository.MessageRepository;
import br.inatel.dashboard.repository.repo.MessageRepositoryImpl;

@Service("messageService")
public class MessageService {

	private MessageRepository messageRepository;
	private MessageRepositoryImpl messageRepositoryImpl;

	@Autowired
	public MessageService(MessageRepository messageRepository, MessageRepositoryImpl messageRepositoryImpl) {
		this.messageRepository = messageRepository;
		this.messageRepositoryImpl = messageRepositoryImpl;
	}

	public List<Message> getAllMessage() {
		return messageRepository.findAll();
	}

	public void addNew(Message message) {
		this.messageRepository.insert(message);
	}

	public List<Message> getDeviceMessage(String deviceName) {

		return messageRepository.findByUsername(deviceName);
	}

	public Message findByUsernameAndEvents(Map<String,String> event) {

		return messageRepository.findByEvents(event);
	}
	
	public List<Message> findExists(String key, String user, int nResults) {

		return messageRepositoryImpl.findByEvents(key, user, nResults);
	}
	
	
	public List<Message> findByUsername(String username, int nResults) {

		return messageRepositoryImpl.findByUsername(username, nResults);
	}

}
