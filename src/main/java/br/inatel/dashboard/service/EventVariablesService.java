package br.inatel.dashboard.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.EventVariables;
import br.inatel.dashboard.repository.EventVariablesRepository;


@Service("trainingMapService")
public class EventVariablesService {

    private EventVariablesRepository eventVariablesRepository;

    @Autowired
    public EventVariablesService(EventVariablesRepository eventVariablesRepository) {
        this.eventVariablesRepository = eventVariablesRepository;
    }
    
    
    
    public Optional<EventVariables> findByUsername(String id) {
    	
    	return eventVariablesRepository.findById(id);
    }
    

}
