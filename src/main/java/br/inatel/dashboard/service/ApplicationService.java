package br.inatel.dashboard.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.Application;
import br.inatel.dashboard.repository.ApplicationRepository;

@Service("applicationService")
public class ApplicationService {

	@Autowired
	private ApplicationRepository applicationRepository;

    
    public void save(Application application) {
    	this.applicationRepository.save(application);
    }
    
    public Optional<Application> findByPublicApplication(String publicApplication) {
    	return this.applicationRepository.findByPublicApplication(publicApplication);
    }

}
