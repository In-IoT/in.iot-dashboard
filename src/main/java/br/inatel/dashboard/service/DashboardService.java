package br.inatel.dashboard.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.Dashboard;
import br.inatel.dashboard.repository.DashboardRepository;

@Service("dashboardService")
public class DashboardService {

	private DashboardRepository dashboardRepository;

	@Autowired
	public DashboardService(DashboardRepository dashboardRepository) {
		this.dashboardRepository = dashboardRepository;
	}

	public void save(Dashboard dashboard) {

		dashboardRepository.save(dashboard);

	}
	
	public void delete(String id) {

		dashboardRepository.deleteById(id);

	}

	public List<Dashboard> listDashboardsFromUser(String belongsTo) {

		return dashboardRepository.findByBelongsTo(belongsTo);

	}
	
	public Optional<Dashboard> findByIdAndBelongsTo(String idDashboard, String belongsTo) {

		return dashboardRepository.findByIdAndBelongsTo(idDashboard, belongsTo);
	}
	
	public Optional<Dashboard> findById(String idDashboard) {

		return dashboardRepository.findById(idDashboard);
	}
	
	public Optional<Dashboard> findByIdAndIsPublic(String idDashboard) {

		return dashboardRepository.findByIdAndIsPublic(idDashboard, true);
	}

}
