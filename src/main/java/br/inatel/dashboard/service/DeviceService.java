package br.inatel.dashboard.service;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.Device;
import br.inatel.dashboard.model.DeviceList;
import br.inatel.dashboard.repository.DeviceRepository;
//import br.inatel.dashboard.repository.repo.DeviceRepository2;
import br.inatel.dashboard.repository.repo.DeviceRepositoryImpl;

@Service("deviceService")
public class DeviceService {

	@Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }
    
    @Autowired
    DeviceRepositoryImpl deviceRepository2;
    
    public Optional<Device> getDevice(String username){
        return deviceRepository.findByUsername(username);
    }

    public List<Device> findByApplication(String application) {
        return deviceRepository.findByApplication(application);
    }
    
    public List<Device> findByGroupAndApplication(String group, String application) {
    	
        return deviceRepository.findByApplicationAndGroups(application, group);
    }
    
    public List<Device> findByGroupAndApplicationAndPublic(String group, String application) {
    	
        return deviceRepository.findByApplicationAndGroupsAndPublicDevice(application, group, true);
    } 
    
    
    public List<Device> findDevicesByApplication(String application) {
        return deviceRepository.findDevicesByApplication(application);
    }
    
    public List<Device> findAll() {
        return deviceRepository.findAll();
    }
    
    public Optional<Device> findByUsernameAndApplication(String username, String application){
        return deviceRepository.findByUsernameAndApplication(username, application);
    }
    
    public Optional<Device> findByIdAndApplication(String id, String application){
        return deviceRepository.findByIdAndApplication(id, application);
    }
    
    public void save(Device device) {
        deviceRepository.save(device);
    }
    
//    public void update(Device device) {
//        deviceRepository.s
//    }
    
    public DeviceList findAbility(String deviceAbility, String application) {

		DeviceList deviceList = new DeviceList();
		List<Device> devices = deviceRepository.findByAbilitiesAndApplication(deviceAbility, application);
		deviceList.setDevices(devices);
		return deviceList;
	}
    
    public Device findAbilityAndOrder(String deviceAbility, String order) {

		DeviceList deviceList = new DeviceList();
		List<Device> devices = deviceRepository2.findByAbilityAndOrder(deviceAbility, order, 30);
		deviceList.setDevices(devices);
		if(deviceList.getDevices().isEmpty())
			return null;
		return deviceList.getDevices().get(0);
	}
}
