package br.inatel.dashboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.UpdateFile;
import br.inatel.dashboard.repository.UpdateFileRepository;


@Service("updateFileService")
public class UpdateFileService {
	
	private UpdateFileRepository demoDocumentRepository;
	
	
	@Autowired
	public UpdateFileService(UpdateFileRepository  demoDocumentRepository) {
		this.demoDocumentRepository =demoDocumentRepository;
	}
	
	public void save(UpdateFile demoDocument) {
		demoDocumentRepository.save(demoDocument);
	}
	
	
	public List<UpdateFile> showDemoDocuments() {
		return demoDocumentRepository.findAll();
	}
	
}
