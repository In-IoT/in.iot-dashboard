package br.inatel.dashboard.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.dashboard.model.User;
import br.inatel.dashboard.repository.UserRepository;

@Service
public class CustomUserDetailsService {

	@Autowired
	private UserRepository userRepository;

	public Optional<User> findUserByEmail(String email) {
	    return userRepository.findByEmail(email);
	}
	
	public String findApplicationByEmaill(String email) {
	    return userRepository.findApplicationByEmail(email).getApplication();
	}
	
	public List<User> listUsersByApplication(String application){
		return userRepository.findByApplication(application);
	}
	

	public void saveUser(User user) {
		//user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		//user.setEnabled(true);
		//Role userRole = roleRepository.findByRole("ADMIN");
		//user.setRoles(new HashSet<>(Arrays.asList(userRole)));
		userRepository.save(user);
	}
	
	public List<User> listUsers() {
		
		return this.userRepository.findAll();
	}

}