package br.inatel.dashboard.controller;

import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import br.inatel.dashboard.model.DocumentList;

public class Program {

	public Program() {
		// TODO Auto-generated constructor stub
	}

	public DocumentList paris(String database2, String dashboardId) {
		try (MongoClient client = new MongoClient("localhost", 27017)) {

			MongoDatabase database = client.getDatabase(database2);
			MongoCollection<Document> collection = database.getCollection("widget");

			// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

			DocumentList doc = new DocumentList();

			Block<Document> processBlock = new Block<Document>() {
				@Override
				public void apply(final Document document) {
					doc.insert(document);
				}
			};

			List<? extends Bson> pipeline = Arrays.asList(
                    new Document()
                            .append("$match", new Document()
                                    .append("dashboardId", dashboardId)
                            ), 
                    new Document()
                            .append("$lookup", new Document()
                                    .append("from", "messages")
                                    .append("localField", "device")
                                    .append("foreignField", "username")
                                    .append("as", "messages")
                            ), 
                    new Document()
                            .append("$project", new Document()
                                    .append("messages._id", 0.0)
                                    .append("_id", 0.0)
                            )
            );
			collection.aggregate(pipeline).allowDiskUse(false).forEach(processBlock);


			return doc;// .get(0);
			// System.out.println("Print collection " + collection);

		} catch (MongoException e) {
			// handle MongoDB exception
		}
		return null;
	}

}