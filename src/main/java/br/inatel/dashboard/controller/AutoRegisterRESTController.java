package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.AUTO_REGISTER_URL_REST;
import static br.inatel.dashboard.constants.Constants.MONGODATABASE;

import java.util.ArrayList;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.model.AutoRegister;
import br.inatel.dashboard.service.AutoRegisterService;

@RestController
public class AutoRegisterRESTController {

	@Autowired
    private AutoRegisterService autoRegisterService;



//    @RequestMapping(value = AUTO_REGISTER_URL, method = RequestMethod.GET)
//    public String autoRegisterPage(Model model) {
//    	
//        //model.addAttribute("message","default");
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        List<Dashboard>dashboards = dashboardService.listDashboardsFromUser(auth.getName());
//		model.addAttribute("dashboards", dashboards);
//        return "configureAutoRegister";
//    }

    @RequestMapping(value = AUTO_REGISTER_URL_REST, method = RequestMethod.POST)
    public AutoRegister autoRegisterConfig(@RequestBody @Valid AutoRegister autoRegister) {

    	
        if (!autoRegister.getUsername().trim().equals("") && !autoRegister.getPassword().trim().equals("")) {
        	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//            String application;
//            application = userService.findApplicationByEmaill(auth.getName());

            autoRegister.setUsername(auth.getName() + "/" + autoRegister.getUsername());
            String application = auth.getDetails().toString();
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(auth.getName(), null,
    				new ArrayList<>());
    		authentication.setDetails(MONGODATABASE);

    		SecurityContextHolder.getContext().setAuthentication(authentication);

            AutoRegister autoRegisterExists = autoRegisterService.findByUsername(autoRegister.getUsername());

            if (autoRegisterExists == null) {
                String password = autoRegister.getPassword();
            	autoRegister.setPassword(password);
                autoRegister.setCreatedDate(new Date());
                autoRegister.setApplication(application);
                autoRegisterService.save(autoRegister);
                
                authentication = new UsernamePasswordAuthenticationToken(auth.getName(), null,
        				new ArrayList<>());
        		authentication.setDetails(application);

        		SecurityContextHolder.getContext().setAuthentication(authentication);
        		
        		autoRegister.setCreatedBy(auth.getName());
                autoRegister.setPassword(password);
                autoRegisterService.save(autoRegister);
                return autoRegister;
            }
            
        }
        return null;
    }
    
//    @RequestMapping(value = "/tryStuff" + "/{dashboardId}", method = RequestMethod.GET)
//    public DocumentList tryStuff(@PathVariable(name = "dashboardId", required = true) String dashboardId) {
//
//    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//    	
//    	widgetService.findByDashboardId(objectId)
//    	
//    	Program program = new Program();
//    	return program.paris(auth.getDetails().toString(),dashboardId);
//    }
}
