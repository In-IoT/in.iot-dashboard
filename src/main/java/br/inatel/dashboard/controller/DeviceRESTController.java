package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.FIND_DEVICES_BY_ABILITY;
import static br.inatel.dashboard.constants.Constants.LIST_DEVICES_REST_URL;
import static br.inatel.dashboard.constants.Constants.LIST_DEVICE_EVENTS_BY_GROUP;
import static br.inatel.dashboard.constants.Constants.MONGODATABASE;
import static br.inatel.dashboard.constants.Constants.NEW_DEVICES_REST_URL;
import static br.inatel.dashboard.constants.Constants.PUBLIC_LIST_DEVICE_EVENTS_BY_GROUP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.model.Application;
import br.inatel.dashboard.model.Device;
import br.inatel.dashboard.model.DeviceList;
import br.inatel.dashboard.model.Message;
import br.inatel.dashboard.service.ApplicationService;
import br.inatel.dashboard.service.DeviceService;
import br.inatel.dashboard.service.MessageService;

@RestController
public class DeviceRESTController {

	@Autowired
	private DeviceService deviceService;

	@Autowired
	private BCryptPasswordEncoder passwordEncryptor;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationService applicationService;

	@RequestMapping(value = LIST_DEVICES_REST_URL, method = RequestMethod.GET)
	public DeviceList listDevices() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String application = auth.getDetails().toString();

		List<Device> devices = deviceService.findByApplication(application);

		DeviceList deviceList = new DeviceList();
		deviceList.setDevices(devices);
		return deviceList;
	}

	@RequestMapping(value = LIST_DEVICES_REST_URL, method = RequestMethod.POST)
	public Device resetDevicePassword(@RequestBody Device device) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String application = auth.getDetails().toString();

		Optional<Device> deviceAux = deviceService.findByUsernameAndApplication(device.getUsername(), application);

		if (!deviceAux.isPresent()) {
			return null;
		}

		String password = PasswordGenerator.generatePassword();

		deviceAux.get().setPassword(passwordEncryptor.encode(password));

		deviceService.save(deviceAux.get());
		deviceAux.get().setPassword(password);
		return deviceAux.get();
	}

	@RequestMapping(value = NEW_DEVICES_REST_URL, method = RequestMethod.POST)
	public Device insertNewDevice(@RequestBody @Valid Device device) {

		device.getAbilities().removeAll(Collections.singleton(""));
		if (device.getAbilities().isEmpty())
			device.setAbilities(null);

		device.getGroups().removeAll(Collections.singleton(""));
		if (device.getGroups().isEmpty())
			device.setGroups(null);

		device.getVariables().removeAll(Collections.singleton(""));
		if (device.getVariables().isEmpty())
			device.setVariables(null);

		if (device.getDescription().isEmpty())
			device.setDescription(null);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String application;
		application = auth.getDetails().toString();

		if (device.getId() != null) {
			Optional<Device> deviceAux = deviceService.findByIdAndApplication(device.getId(), application);
			if (deviceAux.isPresent()) {
				device.setPassword(deviceAux.get().getPassword());
				device.setCreateBy(deviceAux.get().getCreateBy());
				device.setCreateDate(deviceAux.get().getCreateDate());
				device.setUsername(deviceAux.get().getUsername());
				device.setApplication(application);
				device.setDiscoveredVariables(deviceAux.get().getDiscoveredVariables());
				device.setDeleted(deviceAux.get().isDeleted());
				device.setPublicDevice(deviceAux.get().isPublicDevice());
				//device.set
				//deviceAux.get().
				
				deviceService.save(device);
				device.setPassword(null);
				return device;
			}

		}

		Device deviceAux = new Device();
		
		String user = auth.getName();

		deviceAux.setApplication(application);
		deviceAux.setCreateBy(user);
		deviceAux.setCreateDate(new Date());


		String username = UUID.randomUUID().toString();
		deviceAux.setUsername(username);

		String password = PasswordGenerator.generatePassword();

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null,
				new ArrayList<>());
		authentication.setDetails(MONGODATABASE);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		deviceService.save(deviceAux);

		authentication.setDetails(application);

		SecurityContextHolder.getContext().setAuthentication(authentication);

		device.setId(deviceAux.getId());
		device.setPassword(passwordEncryptor.encode(password));
		device.setUsername(username);
		device.setApplication(application);
		device.setCreateBy(user);
		device.setCreateDate(deviceAux.getCreateDate());
		device.setDeleted(false);
		device.setPublicDevice(false);
		device.setDeleted(false);
		device.setPublicDevice(false);
		deviceService.save(device);
		device.setPassword(password);

		return device;
	}

	public Device insertDevice(Device device, String application, String user) {


		return device;

	}

	@RequestMapping(value = LIST_DEVICE_EVENTS_BY_GROUP + "/{groupName}", method = RequestMethod.GET)
	public DeviceList getDeviceByGroup(@PathVariable(name = "groupName", required = true) String groupName,
			Model model) {

		String application = SecurityContextHolder.getContext().getAuthentication().getDetails().toString();

		List<Device> devices = deviceService.findByGroupAndApplication(groupName, application);

		List<Message> messages;

		for (int i = 0; i < devices.size(); i++) {

			messages = messageService.findByUsername(devices.get(i).getUsername(), 30);

			devices.get(i).setMessages(messages);
		}

		DeviceList deviceList = new DeviceList();
		deviceList.setDevices(devices);

		return deviceList;

	}

	@RequestMapping(value = PUBLIC_LIST_DEVICE_EVENTS_BY_GROUP
			+ "/{publicApplication}/{groupName}", method = RequestMethod.GET)
	public DeviceList getPublicDeviceByGroup(@PathVariable(name = "groupName", required = true) String groupName,
			@PathVariable(name = "publicApplication", required = true) String publicApplication, Model model) {

		// Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("empty", null,
				new ArrayList<>());
		authentication.setDetails(MONGODATABASE);

		SecurityContextHolder.getContext().setAuthentication(authentication);

		Optional<Application> application = applicationService.findByPublicApplication(publicApplication);

		if (!application.isPresent())
			return null;

		String application2 = application.get().getApplication();
		authentication.setDetails(application2);

		List<Device> devices = deviceService.findByGroupAndApplicationAndPublic(groupName, application2);

		List<Message> messages;

		for (int i = 0; i < devices.size(); i++) {

			messages = messageService.findByUsername(devices.get(i).getUsername(), 30);

			devices.get(i).setUsername(devices.get(i).getId());
			devices.get(i).setId(null);
			devices.get(i).setMessages(messages);
		}

		DeviceList deviceList = new DeviceList();
		deviceList.setDevices(devices);

		return deviceList;

	}

//    @RequestMapping(value = FIND_DEVICES_BY_ABILITY+"/{deviceAbility}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
//	public DeviceList findAbility(@PathVariable(name = "deviceAbility", required = true) String deviceAbility) {
//
//    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//    	String application = auth.getDetails().toString();
//
//    	return this.deviceService.findAbility(deviceAbility, application);
//    }

	@RequestMapping(value = FIND_DEVICES_BY_ABILITY
			+ "/{deviceAbility}/{order}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Device findAbility(@PathVariable(name = "deviceAbility", required = true) String deviceAbility,
			@PathVariable(name = "order", required = true) String order) {

		return this.deviceService.findAbilityAndOrder(deviceAbility, order);
	}

	@RequestMapping(value = "/findeviceById"
			+ "/{deviceId}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Device findDeviceById(@PathVariable(name = "deviceId", required = true) String deviceId) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();


		// deviceService.findBy
		Optional<Device> device = deviceService.findByIdAndApplication(deviceId, auth.getDetails().toString());
		if (device.isPresent())
			return device.get();
		else
			return null;
	}

}
