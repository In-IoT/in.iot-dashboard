package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.DASHBOARD_DATA_REST_URL;
import static br.inatel.dashboard.constants.Constants.DELETE_DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.PUBLIC_DASHBOARD_DATA_REST_URL;
import static br.inatel.dashboard.constants.Constants.DASHBOARD_REST_URL;
import static br.inatel.dashboard.constants.Constants.GET_DASHBOARDS_FROM_USER_REST_URL;
import static br.inatel.dashboard.constants.Constants.MONGODATABASE;
import static br.inatel.dashboard.constants.Constants.NEW_DASHBOARD_REST_URL;
import static br.inatel.dashboard.constants.Constants.NEW_WIDGET_REST_URL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import br.inatel.dashboard.Feign.Interface.NouvennProxy;
import br.inatel.dashboard.model.ControlLampEntity;
import br.inatel.dashboard.model.Dashboard;
import br.inatel.dashboard.model.DashboardList;
import br.inatel.dashboard.model.Device;
import br.inatel.dashboard.model.Event;
import br.inatel.dashboard.model.Message;
import br.inatel.dashboard.model.Widget;
import br.inatel.dashboard.model.WidgetList;
import br.inatel.dashboard.service.DashboardService;
import br.inatel.dashboard.service.DeviceService;
import br.inatel.dashboard.service.MessageService;
import br.inatel.dashboard.service.WidgetService;

@RestController
public class DashboardActionsRESTController {

	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private NouvennProxy nouvennAPIProxy;

	@Autowired
	private WidgetService widgetService;
	
	@Autowired
	private DeviceService deviceService;

	@Autowired
	private MessageService messageService;

	@RequestMapping(value = GET_DASHBOARDS_FROM_USER_REST_URL, method = RequestMethod.GET)
	public DashboardList getDashboardsFromuser() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<Dashboard> dashboards = dashboardService.listDashboardsFromUser(auth.getName());

		DashboardList dashboardList = new DashboardList();
		dashboardList.setDashboards(dashboards);
		return dashboardList;
	}

	@RequestMapping(value = DASHBOARD_REST_URL + "/{dashboardId}", method = RequestMethod.GET)
	public WidgetList getDashboard(
			@PathVariable(name = "dashboardId", required = true) String dashboardId, Model model) {
		// public ResponseEntity<List<Dashboard>> getDashboardsFromuser(){

		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Optional<Dashboard> dashboard = dashboardService.findByIdAndBelongsTo(dashboardId, auth.getName());
		if (!dashboard.isPresent()) {
			// modelAndView.setViewName("dashboard");
			return null;
		}
		

		//Response<List<Widget>> response = new Response<>();
		List<Widget> widgets = widgetService.findByDashboardId(dashboardId);

		
		for (int i = 0; i < widgets.size(); i++) {
			
			
			List<Message> messages;
			messages = messageService.findExists(widgets.get(i).getVariablesList().get(0), 
					widgets.get(i).getDeviceList().get(0), 30);
			
			widgets.get(i).setMessages(messages);
			widgets.get(i).setDashboard(null);
			//widgets.get(i).setVariable(null);
			widgets.get(i).setUsername(null);;
		}
		// return dashboards;
		//reModifiedWidgetList widgetList = adminAPIProxy2.listWidgetModified(token, dashboardId);sponse.setData(widgets);

		WidgetList widgetList = new WidgetList();
		widgetList.setWidgetList(widgets);
		
		//return ResponseEntity.ok(response);
		return widgetList;

//		List<Widget> widgets = widgetService.findByDashboardId(dashboardId);
//		for (int i = 0; i < widgets.size(); i++) {
//			List<Message> messages;
//			 messages = messageService.findExists(widgets.get(i).getVariable(), widgets.get(i).getDevice());
//		}

		// return dashboards;
		// response.setData(dashboards);

		// return ResponseEntity.ok(response);
		// return ResponseEntity.ok(dashboards);
	}
	
	
	@RequestMapping(value = DASHBOARD_DATA_REST_URL + "/{dashboardId}", method = RequestMethod.GET)
	public WidgetList getDashboardData(
			@PathVariable(name = "dashboardId", required = true) String dashboardId, Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Optional<Dashboard> dashboard = dashboardService.findByIdAndBelongsTo(dashboardId, auth.getName());
		
		
		if (!dashboard.isPresent()) {
			return null;
		}
		
	
		List<Widget> widgets = widgetService.findByDashboardId(dashboardId);
		
		
		
		for (int i = 0; i < widgets.size(); i++) {
			List<Message> messages;
			List<Device> devices = new ArrayList<>();
			if(widgets.get(i).getMaxResults() == null)
			{
				widgets.get(i).setMaxResults(30);
			}
			
			
			if(widgets.get(i).getGroup().isEmpty()) {
				
				
				
				for(int j = 0; j<widgets.get(i).getDeviceList().size() ; j++) {
					
					//System.out.println("Show me this f.... line");
					messages = messageService.findExists(widgets.get(i).getVariablesList().get(j), 
							widgets.get(i).getDeviceList().get(j), widgets.get(i).getMaxResults());
					
					Device device = new  Device();
					
					device.setPlotVariable(widgets.get(i).getVariablesList().get(j));
					device.setUsername(widgets.get(i).getDeviceList().get(j));
					device.setMessages(messages);
					device.setChartType(widgets.get(i).getTypeList().get(j));
					devices.add(device);

					widgets.get(i).setDashboard(null);
					widgets.get(i).setUsername(null);;
				}
				
			} else {
				
				
				devices = deviceService.findByGroupAndApplication(widgets.get(i).getGroup(), auth.getDetails().toString());
				
				
				for (int j = 0; j < devices.size(); j++) {
					
					//System.out.println(devices.get(j).getIp());
					ResponseEntity<String> reply = nouvennAPIProxy.getLatestMessage(devices.get(j).getIp());
				
					if(reply == null) {
						System.out.println("I Overcame the darkness");
					} else {
					//System.out.println(reply);
					Gson g = new Gson();
					ControlLampEntity dto = g.fromJson(((ResponseEntity<String>) reply).getBody(), ControlLampEntity.class); 
					devices.get(j).setControlLampEntity(dto);
					}

				}
				
				Device device = new  Device();
				


				widgets.get(i).setDashboard(null);
				widgets.get(i).setUsername(null);;
				
			}
			
			
			widgets.get(i).setDevices(devices);

		}


		WidgetList widgetList = new WidgetList();
		widgetList.setWidgetList(widgets);
		

		return widgetList;

	}
	
	
	@RequestMapping(value = PUBLIC_DASHBOARD_DATA_REST_URL + "/{dashboardId}", method = RequestMethod.GET)
	public WidgetList getPublicDashboardData(
			@PathVariable(name = "dashboardId", required = true) String dashboardId, Model model) {

		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		SecurityContextHolder.getContext().setAuthentication(null);

		Optional<Dashboard> dashboard = dashboardService.findById(dashboardId);
		
		if (!dashboard.isPresent()) {
			return null;
		}
		
		//SecurityContextHolder.getContext().setAuthentication(auth);
		
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("user", null,
				new ArrayList<>());
		authentication.setDetails(dashboard.get().getApplication());

		SecurityContextHolder.getContext().setAuthentication(authentication);

		List<Widget> widgets = widgetService.findByDashboardId(dashboardId);
		
		for (int i = 0; i < widgets.size(); i++) {
			List<Message> messages;
			List<Device> devices = new ArrayList<>();
			if(widgets.get(i).getMaxResults() == null)
			{
				widgets.get(i).setMaxResults(30);
			}
			for(int j = 0; j<widgets.get(i).getDeviceList().size() ; j++) {
				 
				
				messages = messageService.findExists(widgets.get(i).getVariablesList().get(j), 
						widgets.get(i).getDeviceList().get(j), widgets.get(i).getMaxResults());
				
				Device device = new  Device();
				
				device.setPlotVariable(widgets.get(i).getVariablesList().get(j));
				device.setUsername(widgets.get(i).getPublicDeviceList().get(j));
				device.setMessages(messages);
				device.setChartType(widgets.get(i).getTypeList().get(j));
				devices.add(device);

				widgets.get(i).setDashboard(null);
				widgets.get(i).setUsername(null);;
			}
			
			widgets.get(i).setDeviceList(null);
			widgets.get(i).setDevices(devices);

		}


		WidgetList widgetList = new WidgetList();
		widgetList.setWidgetList(widgets);
		

		return widgetList;

	}
	


	@RequestMapping(value = NEW_DASHBOARD_REST_URL, method = RequestMethod.POST)
	public Dashboard newDashboard(@RequestBody @Valid Dashboard dashboard) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		Dashboard dashboardAux = new Dashboard();
		
		dashboardAux.setIsPublic(dashboard.getIsPublic());
		dashboardAux.setApplication(auth.getDetails().toString());

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(auth.getName(), null,
				new ArrayList<>());
		authentication.setDetails(MONGODATABASE);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		dashboardService.save(dashboardAux);
		
		authentication.setDetails(auth.getDetails());

		SecurityContextHolder.getContext().setAuthentication(authentication);
		dashboard.setId(dashboardAux.getId());
		dashboard.setBelongsTo(auth.getName());
		dashboard.setApplication(auth.getDetails().toString());
		dashboardService.save(dashboard);

		return dashboard;
	}
	
	@RequestMapping(value = DELETE_DASHBOARD_URL + "/{dashboardId}", method = RequestMethod.GET)
	public void deleteDashboard(@PathVariable(name = "dashboardId", required = true) String dashboardId) {

		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		
		Optional<Dashboard> dashboard = dashboardService.findByIdAndBelongsTo(dashboardId, auth.getName());
		
		if(!dashboard.isPresent())
			return;

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(auth.getName(), null,
				new ArrayList<>());
		authentication.setDetails(MONGODATABASE);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		dashboardService.delete(dashboardId);
		
		
		authentication.setDetails(auth.getDetails());

		widgetService.removeWidgetByDashboardId(dashboardId);
		dashboardService.delete(dashboardId);

	}

	@RequestMapping(value = NEW_WIDGET_REST_URL + "/{dashboardId}", method = RequestMethod.POST)
	public Widget newWidget(@RequestBody @Valid Widget widget) {


		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		List<String> publicDeviceList = new ArrayList<>();
		
		if(widget.getGroup().isEmpty()) {
			
			for( int i = 0; i<widget.getDeviceList().size() ; i++ ) {
				Optional<Device> device = deviceService.findByUsernameAndApplication(widget.getDeviceList().get(i), 
						auth.getDetails().toString());
				
				if(!device.isPresent())
					return null;

				publicDeviceList.add(device.get().getId());
			}
			
			if(widget.getMax() == null || widget.getMin() == null || widget.getMax() <= widget.getMin()) {
				widget.setMax(null);
				widget.setMin(null);
			}
			
			if(widget.getMaxResults() == null || widget.getMaxResults()<1 || widget.getMaxResults() >30) {
				widget.setMaxResults(30);
			}
			
			if(widget.getyName().isEmpty()) {
				widget.setyName(null);;
			}
			
		} else {
			
			
		}
		
		
					
		
		widget.setPublicDeviceList(publicDeviceList);

		widgetService.save(widget);
		
		widget.setIdWidget(widget.getId());
		widgetService.save(widget);

		return widget;
	}

}