package br.inatel.dashboard.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.inatel.dashboard.model.UpdateFile;
import br.inatel.dashboard.service.UpdateFileService;

@RestController
public class UploadControllerREST {
	
	
	private UpdateFileService demoDocumentService;

	public UploadControllerREST(UpdateFileService demoDocumentService) {
		this.demoDocumentService = demoDocumentService;
	}
	
	@PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes, 
                                   Double softwareVersion,
                                   String model) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }
        
        
        try {
            UpdateFile demoDocument = new UpdateFile();
            demoDocument.setFile(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
            demoDocument.setModel(model);
            demoDocument.setSoftwareVersion(softwareVersion);
            demoDocumentService.save(demoDocument);
            //mongoTemplate.insert(demoDocument);
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }

        return "success";
    }

    
    @RequestMapping(value = "/uploadStatus", method = RequestMethod.GET)
    public void retrieveFile(HttpServletResponse response){
    	
    	List<UpdateFile> listOfDocuments = demoDocumentService.showDemoDocuments();
        

        Binary document = listOfDocuments.get(0).getFile();
        InputStream myInputStream = new ByteArrayInputStream(document.getData()); 

        String filename = listOfDocuments.get(0).getId();
        response.setHeader("content-disposition", "attachment; filename=\""+filename+"\".bin"); 
        int length = document.getData().length;
        response.setContentType("application/octet-stream");
        response.setContentLength(length);
        response.setContentLengthLong(length);
        
        try {
			IOUtils.copy(myInputStream, response.getOutputStream());
			//System.out.println("Maybe?");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    }

}