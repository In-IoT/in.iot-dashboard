package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.NEW_WIDGET_URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.model.Device;
import br.inatel.dashboard.model.Message;
import br.inatel.dashboard.service.DeviceService;
import br.inatel.dashboard.service.MessageService;



@RestController
public class ComboController {



	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private MessageService messageService;



	@RequestMapping(value = NEW_WIDGET_URL+"/{widgetId}/comboboxChange", method = RequestMethod.POST, produces = "application/json")
	public List<String> comboboxChange(@RequestBody Device device) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();


		Optional<Device> device2 = deviceService.findByUsernameAndApplication(device.getUsername(), auth.getDetails().toString());
		if(!device2.isPresent()) {
			//device2.get().setPassword(null);
			List<String> keys = new ArrayList<>();
			keys.add("no variables to add");
			return keys;
		}
		
		
		
		HashMap<String, String> basicMap = new HashMap<>();

		if(device2.get().getDiscoveredVariables() != null) {
			for (String discoveredVariables : device2.get().getDiscoveredVariables()) 
				basicMap.put(discoveredVariables,"");
		}
		
		List<Message> messages = messageService.findByUsername(device.getUsername(), 10);
		for (Message message : messages) {
			basicMap.putAll(message.getEvents());
		}
		
		List<String> discoveredVariables = new ArrayList<>(basicMap.keySet());
		
		device2.get().setDiscoveredVariables(discoveredVariables);
		
		deviceService.save(device2.get());
		
		//eventVariables
		//List<String> keys = new ArrayList<>(eventVariables.get().getVariables().keySet());
		return discoveredVariables;

 
	}
	
	
}