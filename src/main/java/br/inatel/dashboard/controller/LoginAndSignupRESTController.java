package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.LIST_OTHER_APPLICATION_USERS;
import static br.inatel.dashboard.constants.Constants.SIGN_UP_URL_ADDITIONAL_USER_REST;
import static br.inatel.dashboard.constants.Constants.SIGN_UP_URL_FIRST_USER_REST;
import static br.inatel.dashboard.constants.Constants.VERIFY_JWT;
import static br.inatel.dashboard.constants.Constants.VERIFY_JWT_VALIDITY;
import static br.inatel.dashboard.security.SecurityConstants.SECRETUSER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.model.Application;
import br.inatel.dashboard.model.User;
import br.inatel.dashboard.model.UserList;
import br.inatel.dashboard.service.ApplicationService;
//import br.inatel.dashboard.service.AuthenticationService;
import br.inatel.dashboard.service.CustomUserDetailsService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@RestController
public class LoginAndSignupRESTController {

	private CustomUserDetailsService userService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private ApplicationService applicationService;

	public LoginAndSignupRESTController(BCryptPasswordEncoder bCryptPasswordEncoder,
//										AuthenticationService authService,
			CustomUserDetailsService userService) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userService = userService;
	}

	@RequestMapping(value = SIGN_UP_URL_FIRST_USER_REST, method = RequestMethod.POST, produces= "application/json")
		public User createNewUser(@RequestBody @Valid User user, HttpServletResponse res) throws IOException {

			//if()
			if(user.getEmail() == null || user.getEmail().trim().length()<6) {
				res.sendError(409, "The variable 'email' is not present or length is inferior to 6");
				return null;
			}
			
			if(user.getPassword() == null || user.getPassword().trim().length()<6) {
				res.sendError(409, "The variable 'password' is not present or length is inferior to 6");
				return null;
			}
			
			if(user.getFullname() == null || user.getFullname().trim().length()<6) {
				res.sendError(409, "The variable 'fullname' is not present or length is inferior to 6");
				return null;
			}
			
			
			SecurityContextHolder.getContext().setAuthentication(null);
			user.setEmail(user.getEmail().trim());
			user.setPassword(user.getPassword().trim());
			user.setFullname(user.getFullname().trim());
			Optional<User> userExists = userService.findUserByEmail(user.getEmail());
			if (userExists.isPresent()) {
				res.sendError(409, "Conflict: User Already exists");
				return null;
			}
			
			ObjectId id = new ObjectId();
			
			String application = id.toHexString();
			application = application.replaceAll("....", "$0-");
			if(application.endsWith("-")) {
				application = application.substring(0, application.length()-1);
			}
			
			user.setApplication(application);
			String password = new String(user.getPassword());
			user.setPassword(null);
			
			user.setEnabled(true);
			user.setCreatedDate(new Date());
			
			userService.saveUser(user);
			
			Application application2 = new Application();
			
			String publicApplication = UUID.randomUUID().toString();
	        //device.setUsername(username);
			application2.setApplication(application);
			application2.setPublicApplication(publicApplication);
			
			applicationService.save(application2);
			
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user.getEmail(), null,
					new ArrayList<>());
			authentication.setDetails(application);

			SecurityContextHolder.getContext().setAuthentication(authentication);
			user.setPassword(bCryptPasswordEncoder.encode(password));
			//
			userService.saveUser(user);
			user.setPassword(null);
			user.setId(null);
	        return user;
		}

	@RequestMapping(value = SIGN_UP_URL_ADDITIONAL_USER_REST, method = RequestMethod.POST, produces = "application/json")
	public User createUserWhenLogged(@RequestBody @Valid User user) {

		Optional<User> userExists = userService.findUserByEmail(user.getEmail());
		if (userExists.isPresent()) {
			return null;
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		user.setCreatedBy(auth.getName());
		user.setCreatedDate(new Date());

		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setEnabled(true);
		user.setCreatedDate(new Date());
		String application;
		application = auth.getDetails().toString();
		user.setApplication(application);
		// user.setPassword("");
		userService.saveUser(user);

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user.getEmail(),
				null, new ArrayList<>());
		authentication.setDetails("iniot");

		SecurityContextHolder.getContext().setAuthentication(authentication);
		user.setPassword(null);
		userService.saveUser(user);
		return user;
	}

	@RequestMapping(value = VERIFY_JWT, method = RequestMethod.GET, produces = "application/json")
	public User getUserFromJWT(@RequestHeader @Valid String token) {

		String user = null;
		String application = null;


		try {
			user = Jwts.parser().setSigningKey(SECRETUSER.getBytes())
					// aqui bastaria colocar .parseClaimsJws(token)
					.parseClaimsJws(token).getBody().getSubject();

			application = Jwts.parser().setSigningKey(SECRETUSER.getBytes())
					// aqui bastaria colocar .parseClaimsJws(token)
					.parseClaimsJws(token).getBody().getIssuer();

			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null,
					new ArrayList<>());
			authentication.setDetails(application);

			SecurityContextHolder.getContext().setAuthentication(authentication);


		} catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException
				| IllegalArgumentException e) {
			System.out.println(e);
		}
		Optional<User> usr = userService.findUserByEmail(user);
		if (usr.isPresent()) {
			usr.get().setId(null);
			return usr.get();
		}
		return null;

	}

	@RequestMapping(value = VERIFY_JWT_VALIDITY, method = RequestMethod.GET, produces = "application/json")
	public User isJWTValid() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = new User();
		user.setEmail(auth.getName());
		user.setApplication(auth.getDetails().toString());
		// user.setApplication(application);

		return user;
	}

	@RequestMapping(value = LIST_OTHER_APPLICATION_USERS, method = RequestMethod.GET, produces = "application/json")
	public UserList listOtherApplicationUsers() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String application = auth.getDetails().toString();
		List<User> users = userService.listUsersByApplication(application);

		UserList userList = new UserList();
		userList.setUsers(users);
		return userList;
	}

}