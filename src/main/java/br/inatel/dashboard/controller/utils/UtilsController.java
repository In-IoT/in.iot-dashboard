package br.inatel.dashboard.controller.utils;

import static br.inatel.dashboard.constants.Constants.DASHBOARD_URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.model.Dashboard;
import br.inatel.dashboard.model.Widget;
import br.inatel.dashboard.service.WidgetService;

@RestController
public class UtilsController {
	
	

	private WidgetService widgetService;

	@Autowired 
	public UtilsController(
			WidgetService widgetService) {
		this.widgetService = widgetService;

	}
	
	@RequestMapping(value = DASHBOARD_URL+"/{dashboardId}"+"/resizeDiv", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void resizeDiv(@RequestBody Widget widget,
			@PathVariable(name = "dashboardId", required = true) String dashboardId) {
		
		//widget.setDashboardId(dashboardId);
		
		
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        //widget.setUsername(auth.getName());
        
        widgetService.ediWidgetSize(widget.getId(), widget.getHeight(), widget.getWidth(), auth.getName());



	}
	
	@RequestMapping(value = DASHBOARD_URL+"/{dashboardId}"+"/removeDiv", method = RequestMethod.POST)
	public void removeDiv(@RequestBody Widget widget,
			@PathVariable(name = "dashboardId", required = true) String dashboardId) {
		 
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        
        widgetService.removeWidget(widget.getId(), auth.getName());

	}

}
