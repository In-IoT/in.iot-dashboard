//package br.inatel.dashboard.controller.utils;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.util.List;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.ParseException;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import com.google.gson.Gson;
//
//import br.inatel.dashboard.model.Dashboard;
//
//public class HTTPAuxiliarMethods {
//	private CloseableHttpClient client;
//
//	public HttpResponse doHTTPPOSTRequest(String address, String messageBody, String cookie) {
//
//		StringEntity loginString = null;
//
//		try {
//			loginString = new StringEntity(messageBody);
//		} catch (UnsupportedEncodingException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//
//		client = HttpClients.createDefault();
//
//		HttpPost httpPost = new HttpPost(address);
//		httpPost.setEntity(loginString);
//		httpPost.setHeader("Content-Type", "application/json");
//		HttpResponse httpResponse = null;
//
//		if (cookie != null)
//			httpPost.setHeader("Cookie", cookie);
//
//		try {
//			httpResponse = client.execute(httpPost);
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return httpResponse;
//	}
//
//	public HttpResponse doHTTGETTRequest(String address, String cookie) {
//
//
//		client = HttpClients.createDefault();
//
//		HttpGet httpGet = new HttpGet(address);
//		//httpPost.setEntity(loginString);
//		httpGet.setHeader("Content-Type", "application/json");
//		HttpResponse httpResponse = null;
//
//		if (cookie != null)
//			httpGet.setHeader("Cookie", cookie);
//
//		try {
//			httpResponse = client.execute(httpGet);
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return httpResponse;
//	}
//	
//	
//	public HttpResponse doExternalHTTGETTRequest(String address, String cookie) {
//
//
//		client = HttpClients.createDefault();
//
//		HttpGet httpGet = new HttpGet(address);
//		//httpPost.setEntity(loginString);
//		httpGet.setHeader("Content-Type", "application/json");
//		HttpResponse httpResponse = null;
//
//		if (cookie != null)
//			httpGet.setHeader("token", cookie);
//
//		try {
//			httpResponse = client.execute(httpGet);
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return httpResponse;
//	}
//	
////	@SuppressWarnings("unchecked")
////	public Response<List<Dashboard>> getDashboardList(String address, String header) {
//	public String getDashboardList(String address, String header) {
//		
//		HttpResponse httpResponse = doHTTGETTRequest(address, header);
//		
////		JSONObject jsonObj = null;
//		// httpResponse.get
//
//		String str= null;
//		try {
//			str = EntityUtils.toString(httpResponse.getEntity());
//		} catch (ParseException | IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
////		try {
////			
////			jsonObj = new JSONObject(str);
////			//System.out.println(jsonObj);
////		} catch (JSONException e) {
////			// TODO Auto-generated catch block
////			 e.printStackTrace();
////		} catch (org.apache.http.ParseException e) {
////			// TODO Auto-generated catch block
////			 e.printStackTrace();
////		}
//
//
////			System.out.println(str);
//
//		closeClient();
//		return str;
////		return  (Response<List<Dashboard>>)new Gson().fromJson(jsonObj.toString(), Response.class);
//		
//		
//	}
//	
//	
//	public String getRandomString(String address, String header) {
//		
//		System.out.println(header);
//		HttpResponse httpResponse = doExternalHTTGETTRequest(address, header);
//		
//
//		String str = null;
//		try {
//			str = EntityUtils.toString(httpResponse.getEntity());
//		} catch (ParseException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return str;
//
//		
//		
//	}
//	
//
//	public void closeClient() {
//		try {
//			client.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//}
