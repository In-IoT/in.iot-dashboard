package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.LIST_DEVICE_EVENTS_REST_URL;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.model.Message;
//import br.inatel.dashboard.repository.TestRepo;
import br.inatel.dashboard.service.MessageService;
//import br.inatel.dashboard.service.TestRepoService;

@RestController
public class DeviceEventsRESTController {
	
	@Autowired
	private MessageService messageService;
	
//	@Autowired 
//	private TestRepoService TestRepoService;



    @RequestMapping(value = LIST_DEVICE_EVENTS_REST_URL+"/{deviceUsername}", method = RequestMethod.GET)
    public List<Message> listDeviceEvents(@PathVariable(name = "deviceUsername", required = true) String deviceUsername) {
        return messageService.findByUsername(deviceUsername, 30);
    }
    
//    @RequestMapping(value = "/doThings", method = RequestMethod.GET)
//    public MongoCollection<Document> listDeviceEvents() {
//        return TestRepoService.doThings();
//    }
//    
}
