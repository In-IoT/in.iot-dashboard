package br.inatel.dashboard.repository.repo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;


import br.inatel.dashboard.model.Message;

public class MessageRepositoryImpl implements  MessageRepositoryCustom {


	  private MongoTemplate mongoTemplate;

	  @Autowired
	  public MessageRepositoryImpl(MongoTemplate mongoTemplate) {
	    this.mongoTemplate = mongoTemplate;
	  }

	  @Override
	  public List<Message> findByEvents(String mapId, String username, int nResults) {
		  
		  Query query = new Query();
		  
		  query.limit(nResults);
		  query.with(new Sort(Sort.Direction.DESC, "created_at"));
		  query.fields().exclude("username");
		  query.fields().exclude("_id");
		  query.addCriteria(new Criteria()
				  
				  .andOperator(
				  
				  Criteria.where("username").is(username),
				  Criteria.where("events."+mapId).exists(true))

				  );
		  
		  
		  return mongoTemplate.find(query, Message.class, "messages");
		  //return mongoTemplate.find(query, DocumentClass.class, "DocumentName");

	    
	  }
	  
	  
	  public List<Message> findByUsername(String username, int nResults) {
		  
		  Query query = new Query();
		  
		  query.limit(nResults);
		  query.with(new Sort(Sort.Direction.DESC, "created_at"));
		  query.fields().exclude("username");
		  query.fields().exclude("id");
		  query.addCriteria(new Criteria()
				  
				  .andOperator(
				  
				  Criteria.where("username").is(username))

				  );
		  
		  
		  return mongoTemplate.find(query, Message.class, "messages");
		  //return mongoTemplate.find(query, DocumentClass.class, "DocumentName");

	    
	  }


	}
