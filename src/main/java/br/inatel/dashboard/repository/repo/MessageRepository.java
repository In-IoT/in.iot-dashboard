package br.inatel.dashboard.repository.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.Message;

@Repository("messageRepository2")
public interface MessageRepository extends MongoRepository<Message, String>, MessageRepositoryCustom {

} 