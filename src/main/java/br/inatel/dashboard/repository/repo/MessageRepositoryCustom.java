package br.inatel.dashboard.repository.repo;

import java.util.List;

import br.inatel.dashboard.model.Message;

public interface MessageRepositoryCustom {

	List<Message> findByEvents(final String mapId, String username, int nResults);
	
}