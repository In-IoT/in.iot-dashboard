package br.inatel.dashboard.repository.repo;

import java.util.List;

import br.inatel.dashboard.model.Device;

public interface DeviceRepositoryCustom {

	List<Device> findByAbilityAndOrder(String ability, String order, int nResults);
	
}