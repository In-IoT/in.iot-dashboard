package br.inatel.dashboard.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.Dashboard;

@Repository("dashboardRepository")
public interface DashboardRepository extends MongoRepository<Dashboard, String> {
	
	public List<Dashboard> findByBelongsTo(String belongsTo);
	public Optional<Dashboard> findByIdAndBelongsTo(String id, String belongsTo);
	
	public Optional<Dashboard> findByIdAndIsPublic(String id, boolean isPublic);
	


}