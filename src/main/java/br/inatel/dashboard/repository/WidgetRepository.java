package br.inatel.dashboard.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.Widget;

@Repository("widgetRepository")
public interface WidgetRepository extends MongoRepository<Widget, String> {

	
	public List<Widget> findByDashboardId(String objectId);
	
	public Optional<Widget> findByIdAndUsername(String objectId, String username);
	
	List <Widget> deleteByDashboardId(String dashboardId);

}