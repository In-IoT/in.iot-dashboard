package br.inatel.dashboard.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.UpdateFile;

@Repository("updateFileRepository")
public interface UpdateFileRepository extends MongoRepository<UpdateFile, String> {


}