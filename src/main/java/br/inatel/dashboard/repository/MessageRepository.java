package br.inatel.dashboard.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.Event;
import br.inatel.dashboard.model.Message;

import java.util.List;
import java.util.Map;

@Repository("messageRepository")
public interface MessageRepository extends MongoRepository<Message, String> {

	@Query(fields="{created_at : 1, events : 1,  _id : 0}")
    List<Message> findByUsername(String user);
	
	Message findByEvents(Map<String, String> event); 

//	List<Message> findTop10ByUsernameByOrderByUsernameDesc(String user);
	
	
	

}
