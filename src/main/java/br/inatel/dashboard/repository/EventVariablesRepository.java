package br.inatel.dashboard.repository;


import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.EventVariables;


@Repository("eventVariableRepository")
public interface EventVariablesRepository extends MongoRepository<EventVariables, String> {


	public Optional<EventVariables> findById(String id);
}
