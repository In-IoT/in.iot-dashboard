package br.inatel.dashboard.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import br.inatel.dashboard.model.User;

public interface UserRepository extends MongoRepository<User, String> {

	//@Query(fields="{password : 0}")
    Optional<User> findByEmail(String email);
    
    
    @Query(value="{email : ?0}", fields="{application : 1, _id : 0}")
    User findApplicationByEmail(String email);

    
    List<User> findByApplication(String application);

}