package br.inatel.dashboard.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.Device;


@Repository("deviceRepository")
public interface DeviceRepository extends MongoRepository<Device, String> {

    Optional<Device> findByUsername(String username);
    
    @Query(fields="{ password : 0}")
    List<Device> findByApplication(String application);
    
    @Query(fields="{username : 1, name : 1, createDate : 1, latitude : 1, longitude : 1, ip : 1, _id : 0}")
    List<Device> findByApplicationAndGroups(String application, String group);
    
    @Query(fields="{username : 1, name : 1, createDate : 1, _id : 1}")
    List<Device> findByApplicationAndGroupsAndPublicDevice(String application, String group, boolean publicDevice);
    
    @Query(fields="{username : 1, name : 1,  _id : 0}")
    List<Device> findDevicesByApplication(String application);
    
    Optional<Device> findByUsernameAndApplication(String username, String application);
    
    @Query(fields="{password : 0}")
    Optional<Device> findByIdAndApplication(String id, String application);
    
    @Query(fields="{ _id : 0, password : 0, description : 0, publicDevice: 0, "
    		+ "deleted: 0, createDate : 0, application: 0}")
    List<Device> findByAbilitiesAndApplication(String abilities, String application);
    
}
