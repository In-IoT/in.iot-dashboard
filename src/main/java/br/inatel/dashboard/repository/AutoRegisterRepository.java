package br.inatel.dashboard.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.AutoRegister;

@Repository("autoRegisterRepository")
public interface AutoRegisterRepository extends MongoRepository<AutoRegister, String> {
	
	AutoRegister findByUsername(String username);

}
