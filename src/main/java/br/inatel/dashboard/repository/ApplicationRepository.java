package br.inatel.dashboard.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.dashboard.model.Application;

@Repository("applicationRepository")
public interface  ApplicationRepository extends MongoRepository<Application, String>  {

	Optional<Application> findByPublicApplication(String publicApplication);
}
 