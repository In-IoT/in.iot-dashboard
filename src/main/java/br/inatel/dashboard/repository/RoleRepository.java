package br.inatel.dashboard.repository;


import org.springframework.data.mongodb.repository.MongoRepository;

import br.inatel.dashboard.model.Role;

public interface RoleRepository extends MongoRepository<Role, String> {

    Role findByRole(String role);
}