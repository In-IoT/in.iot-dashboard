package br.inatel.dashboard.security;

import static br.inatel.dashboard.constants.Constants.DASHBOARD_BODY_AUTH;
import static br.inatel.dashboard.constants.Constants.DASHBOARD_HEADER_AUTH;
import static br.inatel.dashboard.constants.Constants.DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.LOGOUT_URL;
import static br.inatel.dashboard.constants.Constants.SIGN_IN_URL;
import static br.inatel.dashboard.constants.Constants.SIGN_UP_URL_FIRST_USER;
import static br.inatel.dashboard.constants.Constants.SIGN_UP_URL_FIRST_USER_REST;
import static br.inatel.dashboard.constants.Constants.PUBLIC_DASHBOARD_DATA_REST_URL;
import static br.inatel.dashboard.constants.Constants.PUBLIC_LIST_DEVICE_EVENTS_BY_GROUP;
import static br.inatel.dashboard.constants.Constants.VERIFY_JWT;
import static br.inatel.dashboard.security.SecurityConstants.SIGN_UP_URL_DEVICES;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import br.inatel.dashboard.repository.UserRepository;


@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserRepository userRepository;

    @Autowired
    public WebSecurity(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository = userRepository;
    }
    
//    @Bean
//	public JWTAuthorizationFilter authenticationTokenFilterBean() throws Exception {
//		return new JWTAuthorizationFilter();
//	}
	
	@Bean
	public AuthenticationManager customAuthenticationManager() throws Exception {
	  return authenticationManager();
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL_DEVICES).permitAll()
                //Start of noJWTAuthURL
                .antMatchers(
                        HttpMethod.GET,
                        SIGN_UP_URL_FIRST_USER,
                        SIGN_IN_URL,
                        PUBLIC_DASHBOARD_DATA_REST_URL+"/**", 
                        PUBLIC_LIST_DEVICE_EVENTS_BY_GROUP+"/**",
                        "/"
                ).permitAll()
                .antMatchers(HttpMethod.GET, "/uploadStatus").permitAll()
                .antMatchers(HttpMethod.GET, LOGOUT_URL).permitAll()
                .antMatchers(HttpMethod.GET, VERIFY_JWT).permitAll()
                //.antMatchers(HttpMethod.POST, LOGIN_PROCESSING_URL).permitAll()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL_FIRST_USER).permitAll()
                .antMatchers(HttpMethod.POST, DASHBOARD_URL).permitAll()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL_FIRST_USER_REST).permitAll()
                .antMatchers(HttpMethod.POST, DASHBOARD_HEADER_AUTH).permitAll()
                .antMatchers(HttpMethod.POST, DASHBOARD_BODY_AUTH).permitAll()
                //End of noJWTAuthURL
                .anyRequest().authenticated()
                .and()
                //.addFilter(
                .addFilter(new JWTAuthenticationFilter(authenticationManager(), this.userRepository))
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                //.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class)
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }
    
}
