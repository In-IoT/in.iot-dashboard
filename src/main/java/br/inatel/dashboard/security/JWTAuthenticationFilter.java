package br.inatel.dashboard.security;

import static br.inatel.dashboard.constants.Constants.LOGIN_PROCESSING_URL;
import static br.inatel.dashboard.security.SecurityConstants.EXPIRATION_TIME;
import static br.inatel.dashboard.security.SecurityConstants.SECRETUSER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.util.JSONParseException;

import br.inatel.dashboard.common.ErrorMessages;
import br.inatel.dashboard.exceptions.UserNotFoundException;
import br.inatel.dashboard.model.Credentials;
import br.inatel.dashboard.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    
    private AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private static final Logger logger = Logger.getLogger("AuthenticationFilter");

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.setFilterProcessesUrl(LOGIN_PROCESSING_URL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
            HttpServletResponse res) throws AuthenticationException {
        logger.info("--- Attempting Authentication ---");
        try {
        	        	
            Credentials credentials = new ObjectMapper()
                    .readValue(req.getInputStream(), Credentials.class);


            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            credentials.getEmail(),
                            credentials.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException | JSONParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
            HttpServletResponse res,
            FilterChain chain,
            Authentication auth) throws IOException {

    	 String username = ((User) auth.getPrincipal()).getUsername();
    	 Optional<br.inatel.dashboard.model.User> applicationUser = userRepository.findByEmail(username);
         //Optional<Device> applicationUser = this.deviceRepository.findByUsernameLogin(username);
         // Building the authentication token & add to ResponseHeader
         String token = Jwts.builder()
                 .setSubject(username)
                 .setIssuer(applicationUser.get().getApplication())
                 .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                 .signWith(SignatureAlgorithm.HS256, SECRETUSER.getBytes())
                 .compact();
	        res.setHeader("application", applicationUser.get().getApplication());
	        res.setHeader("Authorization", token);
         logger.info("--- Successful Authentication ---");

         ServletOutputStream output = res.getOutputStream();

         try {
             

             if (applicationUser.isPresent()) {
            	 applicationUser.get().setPassword(null);
                 output.print(new ObjectMapper().writeValueAsString(applicationUser.get()));
             } else {
                 throw new UserNotFoundException(ErrorMessages.USER_NOT_FOUND);
             }
         } finally {
             output.close();
         }
        
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException failed) throws IOException {
        //ServletOutputStream output = response.getOutputStream();

        response.sendError(HttpStatus.UNAUTHORIZED.value(), ErrorMessages.WRONG_CREDENTIALS);
        //output.print(ErrorMessages.WRONG_CREDENTIALS);
    }
}
