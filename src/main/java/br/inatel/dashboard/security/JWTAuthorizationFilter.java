package br.inatel.dashboard.security;

import static br.inatel.dashboard.security.SecurityConstants.HEADER_STRING;
import static br.inatel.dashboard.security.SecurityConstants.SECRETUSER;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;


//public class JWTAuthorizationFilter extends OncePerRequestFilter {
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {


	public JWTAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}

	@Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
    	
		
		
		
    	String token = req.getHeader(HEADER_STRING)+ ""; // to guarantee that the token is never null
    	
    	
    	
    	UsernamePasswordAuthenticationToken authentication = getAuthentication(token);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    	
    	
        
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {


    			if (token.length() > 125) {

    				String user = null;
    				String application = null;

    				try {

    					Claims claim = Jwts.parser().setSigningKey(SECRETUSER.getBytes())
    							.parseClaimsJws(token).getBody();
    					user = claim.getSubject();
    					
    					application = claim.getIssuer();

    					
    					

    					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = 
    							new UsernamePasswordAuthenticationToken(user,null, new ArrayList<>());
    					
    					usernamePasswordAuthenticationToken.setDetails(application);
    					return usernamePasswordAuthenticationToken;

    				} catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException
    						| IllegalArgumentException e) {
    					 System.out.println(e);
    					 return null;
    				}

    		}
    			return null;
    	
    }
}
