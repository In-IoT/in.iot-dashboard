package br.inatel.dashboard.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "dashboard")
@JsonInclude(value=Include.NON_NULL)
public class Dashboard {

    @Id
    private String id;
    private String name;
    private String belongsTo;
    private String application;
    @DBRef(lazy = true)
    private List<Widget> widget;
    
    private Boolean isPublic;
    public Dashboard(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("belongsTo") String belongsTo,
            @JsonProperty("div") List<Widget> widget
    ) {
        this.id = id;
        this.name = name;
        this.belongsTo = belongsTo;
        this.widget = widget;
    }

    public Dashboard() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public List<Widget> getWidget() {
		return widget;
	}

	public void setWidget(List<Widget> widget) {
		this.widget = widget;
	}

	public String getBelongsTo() {
		return belongsTo;
	}

	public void setBelongsTo(String belongsTo) {
		this.belongsTo = belongsTo;
	}

	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

}
