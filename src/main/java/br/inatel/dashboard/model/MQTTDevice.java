package br.inatel.dashboard.model;


import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(value=com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL)
public class MQTTDevice {
	
	private String email;
	private String fullname;
	private boolean enabled;
	private Date createdDate;
	private String username;
	private String password;
	private String application;
	private String clientID;
	private int nClients;
	
	public int getnClients() {
		return nClients;
	}

	public void setnClients(int nClients) {
		this.nClients = nClients;
	}

	List<String> subscriptionList;
	
	public MQTTDevice() {
		// TODO Auto-generated constructor stub
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public List<String> getSubscriptionList() {
		return subscriptionList;
	}

	public void setSubscriptionList(List<String> subscriptionList) {
		this.subscriptionList = subscriptionList;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
	
	
	

}
