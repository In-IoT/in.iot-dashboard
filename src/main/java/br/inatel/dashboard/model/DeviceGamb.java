package br.inatel.dashboard.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "devices")
public class DeviceGamb {

    @Id
    private String id;
    private String name;
    private String username;
    private String password;
    private String pictureUrl;
    private String type;
    private String createBy;
    private String mac;
    private String ip;
    private String application;
    private String description;
    private List<String> messages;
    private List<String> groups;
    private boolean publicDevice;
    private boolean deleted;
    private Date createDate;
    
    private List<String> deviceVariables;

    public DeviceGamb(
            @JsonProperty("id") String id,
            @JsonProperty("username") String username,
            @JsonProperty("name") String name,
            @JsonProperty("password") String password,
            @JsonProperty("pictureUrl") String pictureUrl,
            @JsonProperty("type") String type,
            @JsonProperty("createBy") String createBy,
            @JsonProperty("mac") String mac,
            @JsonProperty("ip") String ip,
            @JsonProperty("description") String description,
            @JsonProperty("messages") List<String> messages,
            @JsonProperty("deviceVariables") List<String> deviceVariables,
            @JsonProperty("groups") List<String> groups,
            @JsonProperty("publicDevice") boolean publicDevice,
            @JsonProperty("deleted") boolean deleted,
            @JsonProperty("createDate") Date createDate,
            @JsonProperty("application") String application
    ) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.password = password;
        this.pictureUrl = pictureUrl;
        this.type = type;
        this.createBy = createBy;
        this.mac = mac;
        this.ip = ip;
        this.description = description;
        this.messages = messages;
        this.groups = groups;
        this.publicDevice = publicDevice;
        this.deleted = deleted;
        this.createDate = createDate;
        this.deviceVariables = deviceVariables;
    }

    public DeviceGamb() {
		super();
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public boolean isPublicDevice() {
        return publicDevice;
    }

    public void setPublicDevice(boolean publicDevice) {
        this.publicDevice = publicDevice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

	public List<String> getDeviceVariables() {
		return deviceVariables;
	}

	public void setDeviceVariables(List<String> deviceVariables) {
		this.deviceVariables = deviceVariables;
	}

	@Override
	public String toString() {
		return "application";
	}
	
	
    
}
