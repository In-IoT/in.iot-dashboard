package br.inatel.dashboard.model;


import java.util.ArrayList;
import java.util.List;

import org.bson.Document;



public class DocumentList {

	private List<Document> documents;
	
	public DocumentList() {
		documents = new ArrayList<Document>();
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public void insert(Document document) {
		this.documents.add(document);
	}
}
