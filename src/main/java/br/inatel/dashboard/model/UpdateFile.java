package br.inatel.dashboard.model;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "updateFiles")
public class UpdateFile {
	@Id
    @Field
    private String id;

    @Field
    private Binary file;
    
    private String model;
    
    private Double softwareVersion;

	public String getId() {
		return id;
	}

	public Binary getFile() {
		return file;
	}

	public void setFile(Binary file) {
		this.file = file;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Double getSoftwareVersion() {
		return softwareVersion;
	}

	public void setSoftwareVersion(Double softwareVersion) {
		this.softwareVersion = softwareVersion;
	}


	
    
    
}