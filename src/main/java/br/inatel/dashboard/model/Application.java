package br.inatel.dashboard.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "applications")
public class Application {

    @Id
    private String id;
    private String application;
    private String publicApplication;
    
    

    public Application() {

    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getPublicApplication() {
		return publicApplication;
	}

	public void setPublicApplication(String publicApplication) {
		this.publicApplication = publicApplication;
	}

}
