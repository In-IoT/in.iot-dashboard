package br.inatel.dashboard.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "autoRegister")
public class AutoRegister {

    @Id
    private String id;
    private String username;
    private String password;
    private String createdBy;
    private String application;
    private Date createdDate;
    private Date expirationDate;
    

    public AutoRegister() {

    }

    public AutoRegister(
            @JsonProperty("id") String id,
            @JsonProperty("username") String username,
            @JsonProperty("password") String password,
            @JsonProperty("createdBy") String createdBy,
            @JsonProperty("createdDate") Date createdDate,
            @JsonProperty("expirationDate") Date expirationDate,
            @JsonProperty("application") String application) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.expirationDate = expirationDate;
        this.application = application;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}
	
	

}
