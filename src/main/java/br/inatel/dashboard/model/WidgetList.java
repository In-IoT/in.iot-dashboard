package br.inatel.dashboard.model;

import java.util.List;

public class WidgetList {

	private List<Widget> data;

	public List<Widget> getWidgetList() {
		return data;
	}

	public void setWidgetList(List<Widget> data) {
		this.data = data;
	}
	
	
}
