package br.inatel.dashboard.model;


import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eventVariables")
public class EventVariables {
	
	private String id;
	private Map<String, Boolean> variables;
	
	public EventVariables(){
		
	}

	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Map<String, Boolean> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Boolean> variables) {
		this.variables = variables;
	}

}
