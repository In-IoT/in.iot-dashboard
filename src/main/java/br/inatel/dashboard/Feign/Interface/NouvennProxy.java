
package br.inatel.dashboard.Feign.Interface;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.inatel.dashboard.model.ControlLampEntity;
import br.inatel.dashboard.model.Message;

//@FeignClient(name="admin-api")//, url="localhost:8100")
@FeignClient(name="admin-api",url="testapi1.nouvenn.com:3000")
//@RibbonClient(name="admin-api")
public interface NouvennProxy {

	
    


    @RequestMapping(method = RequestMethod.GET,  value = "/[{ipv6Address}]/control/lamp", consumes = "application/json")
    ResponseEntity<String> getLatestMessage(@PathVariable("ipv6Address") String ipv6Address);


}

