//package br.inatel.dashboard.mongo;
//
//import static br.inatel.dashboard.constants.Constants.MONGODATABASE;
//import static br.inatel.dashboard.constants.Constants.MONGOURL;
//
//import java.io.File;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.data.mongodb.MongoDbFactory;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
//import org.springframework.data.mongodb.core.convert.DbRefResolver;
//import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
//import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
//import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
//import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
//import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
//import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
//
//import com.mongodb.MongoClient;
//import com.mongodb.MongoClientURI;
//
//import br.inatel.dashboard.config.FileResourceLoader;
//import br.inatel.dashboard.config.IConfig;
//import br.inatel.dashboard.config.IResourceLoader;
//import br.inatel.dashboard.config.ResourceLoaderConfig;
//
//
//
//
//@Configuration
//@ComponentScan(basePackages = "br.inatel.dashboard")
//@EnableMongoRepositories({"br.inatel.dashboard.repository"})
//@EnableReactiveMongoRepositories
//public class MongoConfiguration {
//
//    @Autowired
//    MongoMappingContext mongoMappingContext;
//    
//    @Autowired
//    Environment env;
//
//    @Bean
//    public MongoDbFactory mongoDbFactory() throws Exception {
//    	
//        String url = MONGOURL;
//        MongoClientURI mongoURI = new MongoClientURI(url);
//        MongoClient mongoClient = new MongoClient(mongoURI);
//        return new SimpleMongoDbFactory(mongoClient, MONGODATABASE);
//    }
//
//    @Bean
//    public MongoTemplate mongoTemplate() throws Exception {
//        
//        //MONGOURL=mongoURL;
//        //MONGODATABASE=mongoDB;
//        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
//        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
//        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
//        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory(), converter);
//        return mongoTemplate;
//    }
//}
