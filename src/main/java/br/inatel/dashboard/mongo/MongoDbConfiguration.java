package br.inatel.dashboard.mongo;


import com.mongodb.MongoClientURI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import static br.inatel.dashboard.constants.Constants.MONGOURL;
import static br.inatel.dashboard.constants.Constants.MONGODATABASE;

import java.net.UnknownHostException;

@Configuration
@ComponentScan(basePackages = "br.inatel.dashboard")
@EnableMongoRepositories({"br.inatel.dashboard.repository"})
@EnableReactiveMongoRepositories
public class MongoDbConfiguration {

	
	@Autowired
	MongoMappingContext mongoMappingContext;
	
    @Bean
    @Primary
    public MongoDbFactory mongoDbFactory() throws UnknownHostException {
        MongoClientURI mongoClientURI = new MongoClientURI(MONGOURL + "/"+MONGODATABASE);
    	
    	
//    	MongoClientURI mongoClientURI = new MongoClientURI("mongodb://iniot_user1:1q2w3e4r5t@104.196.159.188:14107/admin");
    	//#mongodb+srv://iotlab:316S7HKjvFXiSHkvdWiAee@cluster0-065g.mongodb.net
    	//	mongodb+srv://iniot_user1:1q2w3e4r5t@104.196.159.188:14107/admin
        //mongoClientURI.
    	//MUL
        return new MultiTenantMongoDbFactory(mongoClientURI);
    }

    @Bean
    @Primary
    public MongoTemplate mongoTemplate() throws UnknownHostException {
    	
      DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
      MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
      converter.setTypeMapper(new DefaultMongoTypeMapper(null));
      MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory(), converter);
      return mongoTemplate;
        //return new MongoTemplate(mongoDbFactory());
    }
    
//  @Bean
//  public MongoTemplate mongoTemplate() throws Exception {
//      DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
//      MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
//      converter.setTypeMapper(new DefaultMongoTypeMapper(null));
//      MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory(), converter);
//      return mongoTemplate;
//  }
}